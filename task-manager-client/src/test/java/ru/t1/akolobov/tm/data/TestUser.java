package ru.t1.akolobov.tm.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.model.UserDto;

public final class TestUser {

    @NotNull
    public static final UserDto NEW_USER = createUser();

    @NotNull
    private static UserDto createUser() {
        UserDto user = new UserDto();
        user.setLogin("NEW_USER");
        user.setEmail("new_user@email.com");
        return user;
    }

}
