package ru.t1.akolobov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.dto.model.AbstractUserOwnedDtoModel;
import ru.t1.akolobov.tm.enumerated.Sort;

import java.util.List;

public interface IUserOwnedDtoRepository<M extends AbstractUserOwnedDtoModel> extends IDtoRepository<M> {

    void add(@NotNull String userId, @NotNull M model);

    void update(@NotNull String userId, @NotNull M model);

    void clear(@NotNull String userId);

    boolean existById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Sort sort);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    Long getSize(@NotNull String userId);

    void remove(@NotNull String userId, @NotNull M model);

    void removeById(@NotNull String userId, @NotNull String id);

}
